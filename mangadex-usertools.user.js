// ==UserScript==
// @name         MangaDex User Toolkit
// @namespace    https://gitlab.com/m3ch_mania/mangadex-usertools
// @version      0.0105
// @icon         https://mangadex.org/favicon.ico
// @description  Assorted userscript toolkit for MangaDex
// @author       m3ch_mania
// @updateURL    https://gitlab.com/m3ch_mania/mangadex-usertools/raw/master/mangadex-usertools.user.js
// @downloadURL  https://gitlab.com/m3ch_mania/mangadex-usertools/raw/master/mangadex-usertools.user.js
// @include      https://mangadex.org/*
// @include      http://mangadex.org/*
// @include      https://*.mangadex.org/*
// @include      http://*.mangadex.org/*
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// ==/UserScript==

/**
 * Todo list:
 * - Implement proper use of element templates
 * - Merge Panel show/hide functios into toggle function
 *
 */

(function() {

    function mdut_init() {
        (document.querySelector('[title="Settings"]')) ? mdut_addMDv3ConfigMenuItem() : mdut_addMDv2ConfigMenuItem()
        if (/\/manga\/\d+|\/user\/\d+|\/group\/\d+/.test(document.location)) { mdut_showToolkitPanel() }
    }

    function mdut_addMDv3ConfigMenuItem() {
        // Assemble MDUT's Config Menu Item
        const configMenuItem = document.createElement('a')
        configMenuItem.setAttribute('id', 'mdut_li_config')
        configMenuItem.setAttribute('href', '#')
        configMenuItem.classList.add('dropdown-item')
        // Obtain insertion context
        const mdUserSettingsButton = document.querySelector('[title="Settings"]').parentNode
        // Add child elements
        const menuItemChildSpan = document.createElement('span')
        const menuItemChildText = document.createTextNode(' MDUT')
        menuItemChildSpan.classList.add('fas', 'fa-wrench', 'fa-fw')
        menuItemChildSpan.setAttribute('aria-hidden', 'true')
        menuItemChildSpan.setAttribute('title', 'MDUT Config')
        configMenuItem.appendChild(menuItemChildSpan)
        // Add listener for Config Menu
        configMenuItem.addEventListener('click', function(event) {
            mdut_showConfigPanel()
        })
        // Insert Config Menu item in dropdown list
        mdut_insertAfter(menuItemChildText, menuItemChildSpan)
        mdut_insertAfter(configMenuItem, mdUserSettingsButton)
    }

    function mdut_addMDv2ConfigMenuItem() {
        // Assemble MDUT's Config Menu Item
        const configMenuItem = document.createElement('li')
        configMenuItem.setAttribute('id', 'mdut_li_config')
        // Obtain insertion context
        const mdUserSettingsButton = document.getElementById('settings')
        // Add child elements
        const menuItemChildLink = document.createElement('a')
        const menuItemChildSpan = document.createElement('span')
        const menuItemChildText = document.createTextNode(' MDUT')
        menuItemChildLink.setAttribute('href', '#')
        menuItemChildSpan.classList.add('fas', 'fa-wrench', 'fa-fw')
        menuItemChildSpan.setAttribute('aria-hidden', 'true')
        menuItemChildSpan.setAttribute('title', 'MDUT Config')
        configMenuItem.appendChild(menuItemChildLink)
        menuItemChildLink.appendChild(menuItemChildSpan)
        // Add listener for Config Menu
        menuItemChildLink.addEventListener('click', function(event) {
            mdut_showConfigPanel()
        })
        // Insert Config Menu item in dropdown list
        mdut_insertAfter(menuItemChildText, menuItemChildSpan)
        mdut_insertAfter(configMenuItem, mdUserSettingsButton)
    }

    function mdut_insertAfter(newNode, referenceNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling)
    }

    function mdut_showConfigPanel() {
        (document.getElementById('mudt_config_menu')) ? mdut_hideConfigPanel() : mdut_buildConfigMenu()
    }

    function mdut_showToolkitPanel() {
        (document.getElementById('mudt_toolkit_main')) ? mdut_hideToolkitPanel() : mdut_buildUserToolkitPanel()
    }

    function mdut_hideConfigPanel() {
        const configMenuPanel = document.getElementById('mudt_config_menu')
        configMenuPanel.remove()
    }

    function mdut_hideToolkitPanel() {
        const userToolkitMainPanel = document.getElementById('mudt_toolkit_main')
        userToolkitMainPanel.remove()
    }

    function mdut_buildConfigMenu() {
        const configMenuPanel = document.createElement('div')
        configMenuPanel.setAttribute('id', 'mudt_config_menu')
        configMenuPanel.classList.add('panel', 'panel-default')

        //
        // #region Config Menu Panel Header
        //
            const configMenuPanelHeading = document.createElement('div')
            configMenuPanelHeading.classList.add('panel-heading')
            configMenuPanel.appendChild(configMenuPanelHeading)

            const configMenuHeadingTitle = document.createElement('h3')
            configMenuHeadingTitle.classList.add('panel-title')
            configMenuPanelHeading.appendChild(configMenuHeadingTitle)

            const configMenuTitleSpan = document.createElement('span')
            configMenuTitleSpan.classList.add('fas', 'fa-wrench', 'fa-fw')
            configMenuTitleSpan.setAttribute('aria-hidden', 'true')
            configMenuTitleSpan.setAttribute('title', '')
            configMenuHeadingTitle.appendChild(configMenuTitleSpan)

            const configMenuTitleText = document.createTextNode(' MDUT Config Panel')
            mdut_insertAfter(configMenuTitleText, configMenuTitleSpan)

            const configMenuCloseSpan = document.createElement('span')
            configMenuCloseSpan.setAttribute('title', 'Close Config Panel')
            configMenuCloseSpan.style.cursor = 'pointer'
            configMenuCloseSpan.classList.add('minimize', 'fa', 'fa-times', 'fa-lg', 'pull-right')
            configMenuCloseSpan.addEventListener('click', function(event) {
                mdut_hideConfigPanel()
            })
            configMenuHeadingTitle.appendChild(configMenuCloseSpan)
        //
        // #endregion Config Menu Panel Header
        //

        //
        // Begin: Config Menu Panel Body
        //
            const configMenuPanelBody = document.createElement('div')
            configMenuPanelBody.classList.add('panel-body')
            configMenuPanel.appendChild(configMenuPanelBody)

            const configMenuBodyText = document.createTextNode('Config Panel Body Text')
            configMenuPanelBody.appendChild(configMenuBodyText)
        //
        // End: Config Menu Panel Body
        //

        const mainDivContainer = document.getElementById('content')
        mainDivContainer.insertBefore(configMenuPanel, mainDivContainer.firstChild)

    }

    function mdut_buildUserToolkitPanel() {
        const userToolkitMainPanel = document.createElement('div')
        userToolkitMainPanel.setAttribute('id', 'mudt_toolkit_main')
        userToolkitMainPanel.classList.add('panel', 'panel-default')

        //
        // #region User Toolkit Panel Header
        //
            const userToolkitPanelHeading = document.createElement('div')
            userToolkitPanelHeading.classList.add('panel-heading')
            userToolkitMainPanel.appendChild(userToolkitPanelHeading)

            const userToolkitHeadingTitle = document.createElement('h3')
            userToolkitHeadingTitle.classList.add('panel-title')
            userToolkitPanelHeading.appendChild(userToolkitHeadingTitle)

            const userToolkitTitleSpan = document.createElement('span')
            userToolkitTitleSpan.classList.add('fas', 'fa-magic', 'fa-fw')
            userToolkitTitleSpan.setAttribute('aria-hidden', 'true')
            userToolkitTitleSpan.setAttribute('title', '')
            userToolkitHeadingTitle.appendChild(userToolkitTitleSpan)

            const userToolkitTitleText = document.createTextNode(' MDUT Main Panel')
            mdut_insertAfter(userToolkitTitleText, userToolkitTitleSpan)

            const userToolkitCloseSpan = document.createElement('span')
            userToolkitCloseSpan.setAttribute('title', 'Close User Toolkit Panel')
            userToolkitCloseSpan.style.cursor = 'pointer'
            userToolkitCloseSpan.classList.add('minimize', 'fa', 'fa-times', 'fa-lg', 'pull-right')
            userToolkitCloseSpan.addEventListener('click', function(event) {
                mdut_hideToolkitPanel()
            })
            userToolkitHeadingTitle.appendChild(userToolkitCloseSpan)
        //
        // #endregion User Toolkit Panel Header
        //



        //
        // #region User Toolkit Panel Body
        //
            const userToolkitPanelBody = document.createElement('div')
            userToolkitPanelBody.classList.add('panel-body')
            userToolkitMainPanel.appendChild(userToolkitPanelBody)

            const tkbTable = document.createElement('table')
            tkbTable.classList.add('table', 'table-striped', 'table-bordered')
            userToolkitPanelBody.appendChild(tkbTable)

            const tkbTableBody = document.createElement('tbody')
            tkbTable.appendChild(tkbTableBody)

            const tkbTableBodyRow_ChapterActions = document.createElement('tr')
            tkbTableBodyRow_ChapterActions.setAttribute('id', 'mdut_tktb_chapterActions')
            tkbTableBody.appendChild(tkbTableBodyRow_ChapterActions)

            const tbrthChapterActions = document.createElement('th')
            tbrthChapterActions.style.width = '150px'
            tbrthChapterActions.innerText = 'Chapter Actions:'
            tkbTableBodyRow_ChapterActions.appendChild(tbrthChapterActions)

            const tbrtdChapterActions = document.createElement('td')
            tkbTableBodyRow_ChapterActions.appendChild(tbrtdChapterActions)

            const tbrtdChapterActionsBtn = document.createElement('button')
            tbrtdChapterActionsBtn.classList.add('btn', 'btn-danger')
            tbrtdChapterActionsBtn.setAttribute('id', 'mdut_tktb_removeMyChapters')
            tbrtdChapterActionsBtn.addEventListener('click', function (event) {
                mdut_actionMarkChaptersForRemoval()
            })
            tbrtdChapterActions.appendChild(tbrtdChapterActionsBtn)

            const chapterActionsBtnSpanIcon = document.createElement('span')
            chapterActionsBtnSpanIcon.classList.add('fas', 'fa-trash', 'fa-fw')
            chapterActionsBtnSpanIcon.setAttribute('aria-hidden', 'true')
            chapterActionsBtnSpanIcon.setAttribute('title', 'Remove My Chapters')
            tbrtdChapterActionsBtn.appendChild(chapterActionsBtnSpanIcon)

            const chapterActionsBtnSpanText = document.createElement('span')
            chapterActionsBtnSpanText.classList.add('visible-md-inline', 'visible-lg-inline')
            chapterActionsBtnSpanText.innerText = 'Remove my chapters'
            tbrtdChapterActionsBtn.appendChild(chapterActionsBtnSpanText)

            const userToolkitMsgContainer = document.createElement('div')
            userToolkitMsgContainer.classList.add('alert')
            userToolkitMsgContainer.style.display = 'none'
            userToolkitMsgContainer.setAttribute('id', 'mdut_tk_msg')
            userToolkitMsgContainer.setAttribute('role', 'alert')
            userToolkitPanelBody.appendChild(userToolkitMsgContainer)
        //
        // #endregion User Toolkit Panel Body
        //

        const mainDivContainer = document.getElementById('content')
        mdut_insertAfter(userToolkitMainPanel, mainDivContainer.firstChild.nextSibling)

    }

    function mdut_actionMarkChaptersForRemoval() {
        const activeUser = document.querySelector("span[title=Profile]").parentNode.href.match(/^.*\/user\/(\d+)\/?.*$/)[1]
        const chapterList = document.querySelectorAll("tr[id^=chapter]")
        const chaptersByActiveUser = document.querySelectorAll(`tr[id^=chapter] a[href*='/user/${activeUser}/']`)

        document.getElementById('mdut_tktb_removeMyChapters').disabled = true

        for (let i=0;i<chaptersByActiveUser.length;++i) {
            chaptersByActiveUser[i].parentNode.parentNode.classList.add('danger', 'mdut-marked-for-removal')
        }

        // #region GUI Construction

        const tkMsgContainer = document.getElementById('mdut_tk_msg')
        const tkMsgHeaderDiv = document.createElement('div')
        tkMsgContainer.appendChild(tkMsgHeaderDiv)

        const tkMsgHeaderStrong = document.createElement('strong')
        tkMsgHeaderDiv.appendChild(tkMsgHeaderStrong)

        const tkMsgHeaderSpan = document.createElement('span')
        tkMsgHeaderSpan.classList.add('fas', 'fa-info-circle', 'fa-fw')
        tkMsgHeaderSpan.setAttribute('aria-hidden', 'true')
        tkMsgHeaderSpan.setAttribute('title', 'Confirm Action')
        tkMsgHeaderStrong.appendChild(tkMsgHeaderSpan)

        const tkMsgHeaderText = document.createTextNode(' Confirm: ')
        tkMsgHeaderStrong.appendChild(tkMsgHeaderText)

        const tkMsgHeaderPrompt = document.createTextNode('The highlighted chapters will be deleted. Continue?')
        tkMsgHeaderDiv.appendChild(tkMsgHeaderPrompt)

        const tkMsgBodyBr = document.createElement('br')
        tkMsgContainer.appendChild(tkMsgBodyBr)

        const tkMsgBodyDiv = document.createElement('div')
        tkMsgContainer.appendChild(tkMsgBodyDiv)

        const tkMsgBodyBtnDelete = document.createElement('button')
        tkMsgBodyBtnDelete.classList.add('btn', 'btn-danger')
        tkMsgBodyBtnDelete.innerText = 'Delete highlighted chapters'
        tkMsgBodyBtnDelete.addEventListener('click', function(event) {
            mdut_actionRemoveChaptersMarkedForRemoval()
        })
        tkMsgBodyDiv.appendChild(tkMsgBodyBtnDelete)

        const tkMsgBodyBtnCancel = document.createElement('button')
        tkMsgBodyBtnCancel.classList.add('btn', 'btn-info')
        tkMsgBodyBtnCancel.innerText = 'Cancel'
        tkMsgBodyBtnCancel.addEventListener('click', function(event) {
            mdut_actionClearChaptersMarkedForRemoval()
        })
        tkMsgBodyDiv.appendChild(tkMsgBodyBtnCancel)

        tkMsgContainer.classList.add('alert-info', 'text-center')
        tkMsgContainer.style.display = 'block'

        // #endregion
    }

    function mdut_actionClearChaptersMarkedForRemoval() {
        const chaptersMarkedForRemoval = document.querySelectorAll("tr[id^=chapter].mdut-marked-for-removal")

        for (let i=0;i<chaptersMarkedForRemoval.length;++i) {
            chaptersMarkedForRemoval[i].classList.remove('danger', 'mdut-marked-for-removal')
        }

        const tkMsgContainer = document.getElementById('mdut_tk_msg')
        tkMsgContainer.classList.remove('alert-info', 'text-center')
        tkMsgContainer.style.display = 'none'
        tkMsgContainer.innerHTML = null

        document.getElementById('mdut_tktb_removeMyChapters').disabled = false
    }

    function mdut_actionRemoveChaptersMarkedForRemoval() {

        // #region GUI Construction

        const tkMsgContainer = document.getElementById('mdut_tk_msg')

        tkMsgContainer.classList.remove('alert-info', 'text-center')
        tkMsgContainer.style.display = 'none'
        tkMsgContainer.innerHTML = null

        const tkMsgHeaderDiv = document.createElement('div')
        tkMsgContainer.appendChild(tkMsgHeaderDiv)

        const tkMsgHeaderStrong = document.createElement('strong')
        tkMsgHeaderDiv.appendChild(tkMsgHeaderStrong)

        const tkMsgHeaderSpan = document.createElement('span')
        tkMsgHeaderSpan.classList.add('fas', 'fa-cog', 'fa-spin', 'fa-fw')
        tkMsgHeaderSpan.setAttribute('aria-hidden', 'true')
        tkMsgHeaderSpan.setAttribute('title', 'Processing Request')
        tkMsgHeaderStrong.appendChild(tkMsgHeaderSpan)

        const tkMsgHeaderText = document.createTextNode(' Processing Request: ')
        tkMsgHeaderStrong.appendChild(tkMsgHeaderText)

        const tkMsgHeaderStatusContainer = document.createElement('span')
        tkMsgHeaderDiv.appendChild(tkMsgHeaderStatusContainer)

        const tkMsgHeaderItemStatus = document.createTextNode('Removing item ')
        tkMsgHeaderStatusContainer.appendChild(tkMsgHeaderItemStatus)

        const tkMsgHeaderItemCurrent = document.createElement('span')
        tkMsgHeaderItemCurrent.setAttribute('id', 'mdut_item_current')
        tkMsgHeaderItemCurrent.innerText = '?'
        tkMsgHeaderStatusContainer.appendChild(tkMsgHeaderItemCurrent)

        const tkMsgHeaderItemDivider = document.createTextNode(' of ')
        tkMsgHeaderStatusContainer.appendChild(tkMsgHeaderItemDivider)

        const tkMsgHeaderItemTotal = document.createElement('span')
        tkMsgHeaderItemTotal.setAttribute('id', 'mdut_item_total')
        tkMsgHeaderItemTotal.innerText = '?'
        tkMsgHeaderStatusContainer.appendChild(tkMsgHeaderItemTotal)

        tkMsgContainer.classList.add('alert-info', 'text-center')
        tkMsgContainer.style.display = 'block'

        // #endregion

        const chaptersMarkedForRemoval = document.querySelectorAll("tr[id^=chapter].mdut-marked-for-removal")

        tkMsgHeaderItemTotal.innerText = chaptersMarkedForRemoval.length
        tkMsgHeaderItemCurrent.innerText = '0'

        for (let i=0;i<chaptersMarkedForRemoval.length;++i) {
            let chapterIdToRemove = chaptersMarkedForRemoval[i].id.match(/^chapter_(\d+)$/)[1]
            try {
                mdut_actionRemoveChapter(chapterIdToRemove)
                chaptersMarkedForRemoval[i].remove()
                tkMsgHeaderItemCurrent.innerText = `${i+1}`
                continue
            }
            catch (error) {
                // tfw Holo returns an HTTP 200 even if it fails
                // why even error?
            }
            finally {
                console.info(`MDUT: BulkChapterDelete - Finished removal attempt for Chapter ID: ${chapterIdToRemove}`)
            }
        }

        document.getElementById('mdut_tktb_removeMyChapters').disabled = false

        //window.setTimeout(function() {
        //    mdut_fadeOutEffect(messageContainer, 50)
        //}, 3000)

    }

    function mdut_actionRemoveChapter(chapterID) {
        const mangadexAjaxPath = '/ajax/actions.ajax.php?function=chapter_delete&id='

        // Copied from elsewhere - Need to finish integration
        fetch(`${mangadexAjaxPath}${chapterID}`, {
            cache: 'no-cache',
            credentials: 'include',
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            method: 'POST'
        })
        .then(function(response) {
            if(response.ok) {
                console.info(`MDUT: BulkChapterDelete - Sent delete request for chapter: ${chapterID}`)
            } else {
                throw new Error(response.text())
            }
        })
        .catch(function(error){
            console.error(`MDUT: BulkChapterDelete - Error: ${error.message}`)
           // tfw Holo returns an HTTP 200 even if it fails
           // why even error?
        })
    }

    function mdut_fadeOutEffect(fadeTarget, fadeDelay, redirect = false) {
        const fadeEffect = setInterval(function() {
            if(!fadeTarget.style.opacity) {
                fadeTarget.style.opacity = 1
            }
            if (fadeTarget.style.opacity > 0) {
                fadeTarget.style.opacity -= 0.1
            } else {
                clearInterval(fadeEffect)
                fadeTarget.innerHTML = null
                fadeTarget.style.opacity = 1
                fadeTarget.style.display = 'none'
                if (redirect) {
                    location.href = redirect
                }
            }
        }, fadeDelay)
    }

    // init
    mdut_init()

})();
